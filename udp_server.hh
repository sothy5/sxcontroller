/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */



#pragma once

#include "ngic/cp/gtp_main.h"
#include "./sx/client_wrapper.h"
#include "sx/sx_association.h"


#include "core/app-template.hh"
#include "core/future-util.hh"
#include "core/sleep.hh"
#include "net/packet-data-source.hh"
#include "./udp_client.hh"
#include "./ngic/cp_dp_api/vepc_cp_dp_api.h"


using namespace seastar;
using namespace net;
using namespace std::chrono_literals;

class udp_server {
private:
    udp_channel _chan;
    timer<> _stats_timer;
    uint64_t _n_sent {};
    char buffer[1024]={};
    lw_shared_ptr<distributed<udp_client>> _client_dist;    

   


public:
      //udp_server(){}
      udp_server(lw_shared_ptr< distributed<udp_client>> client_dist): _client_dist(client_dist){}   
 
   
    future<> start(uint16_t port) {
        //uint16_t port=2123;
        ipv4_addr listen_addr{port};
        _chan = engine().net().make_udp_channel(listen_addr);
        std::cout<<"cpu id"<< engine().cpu_id()<< std::endl;

        _stats_timer.set_callback([this] {
            std::cout << "Out: " << _n_sent << " pps" << std::endl;
            _n_sent = 0;
        });
        _stats_timer.arm_periodic(1s);
        std::cout<<"Before "<< std::endl;
        std::cout <<"after"<< std::endl;
        
        gtp_init();
         
        return keep_doing([this] {
            return _chan.receive().then([this] (udp_datagram dgram) {
                //memset(buffer,0,1024);
                auto is=input_stream<char>( data_source(std::make_unique<packet_data_source>(std::move(dgram.get_data()))));
                is.read().then([this] (temporary_buffer<char> data){
                               //gtpv2c_header *gtpv2c_rx = (gtpv2c_header *)((char* ) data.get());
                               //gtpv2c_header *gtpv2c_tx = (gtpv2c_header *) tx_buf;
                               char tx_buf[1024]={};
                               struct session_info session;
                               memset(&session,0, sizeof(session));

                               gtp_main((char *)data.get(), tx_buf, &session);
                               //uint64_t arg=10; 
                                std::cout<< "client calling" << std::endl;
                               _client_dist->invoke_on_all(&udp_client::session_create, session);
                                std::cout<< "client finishing" << std::endl;
                               return make_ready_future<>();
                               });
                
                 std::cout<< "_n_sent" << std::endl;
                _n_sent++;
                });
               

            });
        }

      
          /*
          return smp::submit_to(1, [this, func, args]() mutable {
                 return apply([this, func](Args&&... args) mutable {
                       auto inst= _instances[engine().cpu_id()].service;
                       if (inst) {
                             return ((*inst).*func)(std::forward<Args>(args)..;);

                        }else {
                            throw no_sharded_instance_exception();
                        }
                        },std::move(args));  
                    }); 
             */
     



    future<> stop() { 
       return make_ready_future<>(); 

     }
};

