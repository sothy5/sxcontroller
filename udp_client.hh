/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */



#pragma once

#include "core/app-template.hh"
#include "core/future-util.hh"
#include "core/reactor.hh"
#include "net/api.hh"
#include "net/packet-data-source.hh"
#include <iostream>
#include "sx/sx_association.h"
#include "./sx/client_wrapper.h"
#include "util/print_safe.hh"
#include "sx/sx_session.h"

using namespace seastar;
using namespace net;
using namespace std::chrono_literals;

class udp_client {
private:
    udp_channel _chan;
    uint64_t n_sent {};
    uint64_t n_received {};
    uint64_t n_failed {};
    timer<> _stats_timer;
    ipv4_addr _server_addr;
    uint8_t status=0; //0=default, 1= association made
    
public:
    
    
  future<> start_and_send(ipv4_addr server_addr, const char* msg ){
           _chan = engine().net().make_udp_channel();
           return _chan.send(server_addr,msg).then([this] (){
                    std::cout<< "data sent" << std::endl;
                    return _chan.receive().then([](udp_datagram data){
                            auto is=input_stream<char>( data_source(std::make_unique<packet_data_source>(std::move(data.get_data()))));
                            is.read().then([] (temporary_buffer<char> data){
                               uint16_t version=(uint16_t)(data.get())[0];
                               std::cout<< "version:"<< version << std::endl;
                               uint16_t  type=(uint16_t)(data.get())[1];
                               std::cout<< "Type:"<< type<< std::endl;
                               return make_ready_future<>();
                            });

                           is.close();
                           std::cout<< "msg recv" << std::endl;
                           return make_ready_future<>();
                     });
		}); 
        }
 future<> start(ipv4_addr server_addr) {
   _chan = engine().net().make_udp_channel();
   
   char buffer[1024]={};
   create_sx_association_setup_request(buffer);
   fragment f{buffer, sizeof(buffer)};
   packet p(f);
   return this->send(server_addr,std::move(p)).then([this] {
             std::cout<< "create_sx_association_setup_request" << std::endl;
             return this->receive().then( [this] (udp_datagram data) {
                    auto is=input_stream<char>( data_source(std::make_unique<packet_data_source>(std::move(data.get_data()))));
                    //TODO: Not good way,
                    this->status=1;
                                       
                    is.read().then([] (temporary_buffer<char> data){
                        uint16_t version=(uint16_t)(data.get())[0];
                        std::cout<< "version:"<< version << std::endl;
                        uint16_t  type=(uint16_t)(data.get())[1];
                        std::cout<< "Type:"<< type<< std::endl;
                        if (type== SX_ASSOCIATION_SETUP_RESPONSE)
                            process_sx_association_setup_response( (char* )data.get());
                        
                        return make_ready_future<>();

             });
          });
       });
    
   


}


future<>  send( ipv4_addr server_addr, const char* msg ){
  std::cout<<"sizeof" << sizeof(msg) << std::endl;
  std::cout<<"string length" << strlen(msg) << std::endl;

 
  pfcp_header* header= (pfcp_header *)msg;
  std::cout<< "msg length" << ntohs(header->pfcp.mlength) << std::endl;
  return _chan.send(server_addr,msg)
                .then_wrapped([this] (auto&& f) {
                    try {
                        f.get();
                        n_sent++;
                    } catch (...) {
                        n_failed++;
                    }
                });


}
future<>  send( ipv4_addr server_addr,  packet msg ){
 // std::cout<<"sizeof" << sizeof(msg) << std::endl;
 // std::cout<<"string length" << strlen(msg) << std::endl;

 
 // pfcp_header* header= (pfcp_header *)msg;
 // std::cout<< "msg length" << ntohs(header->pfcp.mlength) << std::endl;
  return _chan.send(server_addr, std::move(msg))
                .then_wrapped([this] (auto&& f) {
                    try {
                        f.get();
                        n_sent++;
                    } catch (...) {
                        n_failed++;
                    }
                });


}

future<> process( uint64_t x) {
    /*
    char tx_buf[1024]= {0};
    create_sx_session_establishment_request(tx_buf);
    fragment f{tx_buf, sizeof(tx_buf)};
    packet p(f);
    std::cout<< "recv integer"<< x << std::endl;
    return _chan.send(_server_addr, std::move(p));
    */
    std::cout<< "recv integer"<< x << std::endl;
     return make_ready_future<>();

}

future<> session_create ( struct session_info session) {

   char buffer[1024]={};
   create_sx_session_establishment_request(buffer);
   fragment f{buffer, sizeof(buffer)};
   ipv4_addr server_addr("192.168.1.32:8805");

   packet p(f);
   return this->send(server_addr,std::move(p)).then([this] {
             std::cout<< "create_sx_ession_establishment_request" << std::endl;
             return this->receive().then( [this] (udp_datagram data) {
                    auto is=input_stream<char>( data_source(std::make_unique<packet_data_source>(std::move(data.get_data()))));
                                       
                    is.read().then([] (temporary_buffer<char> data){
                        uint16_t version=(uint16_t)(data.get())[0];
                        std::cout<< "version:"<< version << std::endl;
                        uint16_t  type=(uint16_t)(data.get())[1];
                        std::cout<< "Type:"<< type<< std::endl;
                        if (type== SX_SESSION_ESTABLISHMENT_RESPONSE)
                              process_sx_session_establishment_response( (char* )data.get());
                        
                        return make_ready_future<>();

             });
          });
       });
    


}

future<> stop() {
       return make_ready_future<>();

     }



future<udp_datagram> receive(){
   return  _chan.receive();

}

 

           
  };
      
  
