/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */
#include "core/distributed.hh"
#include "core/app-template.hh"
#include "core/future-util.hh"
#include "core/sleep.hh"
#include "./udp_server.hh"
#include "./udp_client.hh"


#include "./sx/client_wrapper.h"
#include "./sx/sx_association.h"
#include "./sx/sx_session.h"

using namespace seastar;
using namespace net;
using namespace std::chrono_literals;




namespace bpo = boost::program_options;

int main(int ac, char ** av) {
    app_template app;
    app.add_options()
        ("port", bpo::value<uint16_t>()->default_value(2123), "UDP server port") ;
    return app.run_deprecated(ac, av, [&] {
        auto&& config = app.configuration();

        uint16_t port = config["port"].as<uint16_t>();
        //lw_shared_ptr<distributed<udp_server>>   server = ::make_lw_shared< distributed<udp_server>>  ();
        auto   client = ::make_lw_shared<distributed<udp_client>>();
        
        //auto server = new distributed<udp_server>();
        //auto client = new distributed<udp_client>();
        
               
    
        ipv4_addr default_addr("192.168.1.32:8805");
        client->start_single().then([client ,default_addr]()  mutable {
            engine().at_exit([client] {
                return client->stop();
            });
            client->invoke_on_all(&udp_client::start, default_addr);
        }).then([default_addr] {
            std::cout << "sx client connect to " << default_addr << " ...\n";
        });
        

        auto server =new distributed<udp_server>();        
 
        server->start_single(client ).then([server=std::move(server), port ] () mutable {
            engine().at_exit([server] {
                return server->stop();
            });
            server->invoke_on_all(&udp_server::start,port);
        }).then([port] {
            std::cout << "Seastar UDP server listening on port " << port << " ...\n";
        });
        

        
        
        
    });
}

