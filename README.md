/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */

# SXCONTROLLER


The architecture of Evolved Packet Core (EPC) is advanced in Release 14 with Control and User Plane Separation of EPC nodes (CUPS). The CUPS introduces a new protocol referred as Sx similar to Openflow protocol for interacting between control and user plane.  The CUPS is applicabale to SGW, PGW and TDF. The source code assumes SPGW-C, control plane of combined SPGW as shown in the architecture section.



##Sx protocol

3GPP TS 29.244 specifies Packet Forwarding Control Protocol (PFCP) as a sx protocol for node and session related procedure.


## Architecture

                               EPC Core
                               +-------------------------+
                               | +------+     +---------+|
                        Control| |  MME | S11 | SPGW-C  ||
                         Signal| |      +-----+         ||
             +---------+  +-------+     |     |         ||
    +--+     |         |  |    | +-----+-     +---+-----+|
    |UE+-----+         |  |    |                 |       |
    +--+     |         |  |    |                 |       |
             |         +--+    |                 |Sx     |
    +--+     |  eNodeB +--+    |                 |       |
    |UE+-----+  (Base  |  |    |                 |       |
    +--+     | Station)|  |    |                 |       |   +--------------+
             |         |  |    |s1u +-------------+  sgi |   | External     |
    +--+     |         |  +---------+ |SPGW-D      | +-------+ Network      |
    |UE+-----+         |  User |    +-------------+      |   |              |
    +--+     +---------+  Data |                         |   |              |
                ||             |                         |   +--------------+
                ||             +-------------------------+
                ||
                ++ 


## How to build 
   use ninja build given in the source code. 

## Dependency
   Seastar (with DPDK or Not)
   NGIC modules. Both open sources 

## Call Flow
   The development intends to implement the INITIAL ATTACH procedure (3GPP 23.401). The development is not complete.   

## Pull request from community.
   if anyone interested to contribute, please feel free to make pull request or contact me. 


















   



