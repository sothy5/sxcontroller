/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */


struct cp_node_id_in_server{
       uint8_t version; /* ipv4=0,ipv6=1 */
       unsigned long s_addr;
       uint8_t cp_function_feature;
};
