/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */



#include "sx_msg.h"
#include "sx_ie.h"
#include <stdio.h>
#include <time.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "./client/client_common.h"
#include "sx_association.h"



int create_sx_association_setup_request(char* msg){
 
       pfcp_header *header=(pfcp_header *) msg;
       header->pfcp.version=PFCP_VERSION;
       header->pfcp.mp=0;
       header->pfcp.s=0;
       header->pfcp.mtype=SX_ASSOCIATION_SETUP_REQUEST;
       header->seid_header.no_seid.seq_num=htonl(sequencenumber)>>8;
       header->pfcp.mlength= header->pfcp.s ?htons(sizeof(header->seid_header.has_seid)) : htons(sizeof(header->seid_header.no_seid));        
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
       printf("str leng: %lu\n", strlen(msg));
    
      /* Sx_association_setup_request */
      /* Node ID, Recovery Time Stamp, UP FUnction Features, CP Function Features, User Plane IP Resource Information */
      
       sx_ie *node_id=(sx_ie *) ( ((uint8_t *) &header->seid_header) +ntohs(header->pfcp.mlength)); 
      
       node_id->type=htons(SX_IE_NODE_ID);
       node_id->length=htons(5);
       
       *((uint8_t *)(&node_id->length)+2)=0;      /* IPv4 address for Node id of controller */
       *((uint8_t *)(&node_id->length)+3)=192;
       *((uint8_t *)(&node_id->length)+4)=168;
       *((uint8_t *)(&node_id->length)+5)=1;
       *((uint8_t *)(&node_id->length)+6)=32;
       
       header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ntohs(node_id->length));
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
       printf("str leng: %lu\n", strlen(msg));
     
       sx_ie *recovery_time_stamp =(sx_ie *) ( ((uint8_t *) &header->seid_header) + ntohs(header->pfcp.mlength));
       
       recovery_time_stamp->type = htons(SX_IE_RECOVERY_TIME_STAMP);
       recovery_time_stamp->length =htons(8) ;
       uint64_t timestamp= (uint64_t)time(NULL);
       
       uint8_t timestamp_value[8]={ (uint8_t)( (timestamp >> 56) & 0xFF),
                                    (uint8_t)( (timestamp >> 48) & 0xFF),
                                    (uint8_t)( (timestamp >> 40) & 0xFF),
                                    (uint8_t)( (timestamp >> 32) & 0xFF),
                                    (uint8_t)( (timestamp >> 24) & 0xFF),
                                    (uint8_t)( (timestamp >> 16) & 0xFF),
                                    (uint8_t)( (timestamp >> 8) & 0xFF),
                                    (uint8_t)( (timestamp & 0xFF)) };
       
       *((uint8_t *)(&recovery_time_stamp->length)+2)=timestamp_value[0];
       *((uint8_t *)(&recovery_time_stamp->length)+3)=timestamp_value[1];
       *((uint8_t *)(&recovery_time_stamp->length)+4)=timestamp_value[2];
       *((uint8_t *)(&recovery_time_stamp->length)+5)=timestamp_value[3];
       *((uint8_t *)(&recovery_time_stamp->length)+6)=timestamp_value[4];
       *((uint8_t *)(&recovery_time_stamp->length)+7)=timestamp_value[5];
       *((uint8_t *)(&recovery_time_stamp->length)+8)=timestamp_value[6];
       *((uint8_t *)(&recovery_time_stamp->length)+9)=timestamp_value[7];        

       header->pfcp.mlength=htons( ntohs(header->pfcp.mlength)+4+ ntohs(recovery_time_stamp->length));
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
       printf("str leng: %lu\n", strlen(msg));
       
       /*        
       sx_ie *up_function_features;
       */
       
       sx_ie *cp_function_features=(sx_ie *) ( ((uint8_t *) &header->seid_header) + ntohs (header->pfcp.mlength));
       cp_function_features->type=htons(SX_IE_CP_FUNCTION_FEATURES);
       cp_function_features->length=htons(1);
       *((uint8_t *)(&cp_function_features->length)+2)=0; /* LOAD and OVRL sets off */
       
       header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ ntohs(cp_function_features->length));
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
       printf("str leng: %lu\n", strlen(msg));

       /*
       sx_ie *up_ip_resource_information;
        */
        return 0; 
}

/*  
int create_sx_association_setup_response(pfcp_header *header) {
       //TODO
       //sx_ie *node_id;
       //sx_ie *cause;
       //sx_ie *recovery_time_stamp;
       //sx_ie *up_function_features;
       //sx_ie *cp_function_features;
       //sx_ie *up_ip_resource_informatin;
     
       return 0;
} 
*/





int process_sx_association_setup_response(char* msg  ){
    pfcp_header *pfcp_header_rx=(pfcp_header *) msg;
    if(pfcp_header_rx->pfcp.version==PFCP_VERSION && pfcp_header_rx->pfcp.mtype==SX_ASSOCIATION_SETUP_RESPONSE){
           
           
            printf("process_sx_association_setup_response\n");
         
            sx_ie *first_ie=PFCP_IE_BEGIN(pfcp_header_rx);
            struct dp_node_id node_id;
            
            if (ntohs(first_ie->type)==SX_IE_NODE_ID ) {
                if(ntohs(first_ie->length)== 5 && *((uint8_t *)(&first_ie->length)+2)==0){
                   
                    uint8_t ip[]={*((uint8_t *)(&first_ie->length)+3) , *((uint8_t *)(&first_ie->length)+4) , *((uint8_t *)(&first_ie->length)+5) , *((uint8_t *)(&first_ie->length)+6)};
                  
                   //struct inet_addr node1=inet_addr(ip);
                   
                   node_id.version=0;
                   inet_pton(AF_INET, (char *)ip, &node_id.s_addr);
                   sx_ie *second_ie=PFCP_NEXT_IE(first_ie);
                   if (second_ie->type==SX_IE_CAUSE){
                       node_id.is_associated= *((uint8_t *)(&second_ie->length)+2);
                   }

                   sx_ie *recovery_time=PFCP_NEXT_IE(second_ie);
                   sx_ie *up_function_features=PFCP_NEXT_IE(recovery_time);
                   
                   node_id.fifth_bit_up_fun_features=*((uint8_t *) (&up_function_features->length)+2);
                   node_id.sixth_bit_up_fun_features=*((uint8_t *) (&up_function_features->length)+3);

                   /* */
                   
                   struct up_ip_resource_information up_res_inf;
                   up_res_inf.ip_version=1;
                   up_res_inf.TEIDRI=0;
                   up_res_inf.ASSONI=1;
                   up_res_inf.has_address.ip_v4[0]=192;
                   up_res_inf.has_address.ip_v4[1]=168;
                   up_res_inf.has_address.ip_v4[2]=1;
                   up_res_inf.has_address.ip_v4[3]=32;
                   memcpy(up_res_inf.network_instance,"internet.mnc012.mcc345.gprs", strlen("internet.mnc012.mcc345.gprs"));
                   
                   node_id.up_res_infor=up_res_inf;
                   dp_node_list= &node_id;
                    
                   printf("Processed sx_assocation_setup...... \n" );
                }
            }
         
          
    }
    return 0;
    
}



  
