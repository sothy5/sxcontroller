/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */


#include "client_wrapper.h"
#include <arpa/inet.h>
#include "./client/client_common.h"
#include <stdio.h>


void get_sx_heart_beat_msg( char* msg){
      //char buffer[1024];
      pfcp_header *header=(pfcp_header *) msg;
      header->pfcp.version=PFCP_VERSION;
      header->pfcp.mp=0;
      header->pfcp.s=0;
      header->pfcp.mtype=SX_HEARTBEAT_REQUEST;
      header->seid_header.no_seid.seq_num=htonl(sequencenumber)>>8;
      //htonl( 

      header->pfcp.mlength= header->pfcp.s ?htons(sizeof(header->seid_header.has_seid)) : htons(sizeof(header->seid_header.no_seid));
      //set_recovery_ie(header);
      printf("buffer filled\n");     
      printf("message size: %d\n", ntohs(header->pfcp.mlength)); 


}


