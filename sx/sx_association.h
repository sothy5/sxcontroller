/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */



#ifdef __cplusplus
extern "C" {
#endif

int create_sx_association_setup_request(char* msg);
int process_sx_association_setup_response(char* msg);

#ifdef __cplusplus
}
#endif

