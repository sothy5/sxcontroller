/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */


#ifndef __Sx_IE_H__
#define __Sx_IE_H__

#include <stdint.h>

/* Information Element type values according to 3GPP TS 29.244 Table 8.1.2-1  */

#define SX_IE_RESERVER                             (0)
#define SX_IE_CREATE_PDR                           (1)
#define SX_IE_PDI                                  (2)
#define SX_IE_CREATE_FAR                           (3)
#define SX_IE_FORWARDING_PARAMETERS                (4)
#define SX_IE_DUPLICATING_PARAMETERS               (5)
#define SX_IE_CREATE_URR                           (6)
#define SX_IE_CREATE_QER                           (7)
#define SX_IE_CREATED_PDR                          (8)
#define SX_IE_UPDATE_PDR                           (9)
#define SX_IE_UPDATE_FAR                           (10)
#define SX_IE_UPDATE_FORWARDING_PARAMETERS         (11)
#define SX_IE_UPDATE_BAR                           (12)
#define SX_IE_UPDATE_URR                           (13)
#define SX_IE_UPDATE_QER                           (14)
#define SX_IE_REMOVE_PDR                           (15)
#define SX_IE_REMOVE_FAR                           (16)
#define SX_IE_REMOVE_URR                           (17)
#define SX_IE_REMOVE_QER                           (18)
#define SX_IE_CAUSE                                (19)
#define SX_IE_SOURCE_INTERFACE                     (20)
#define SX_IE_F_TEID                               (21)
#define SX_IE_NETWORK_INSTANCE                     (22)
#define SX_IE_SDF_FILTER                           (23)
#define SX_IE_APPLICATION_ID                       (24)
#define SX_IE_GATE_STATUS                          (25)
#define SX_IE_MBR                                  (26)
#define SX_IE_GBR                                  (27)
#define SX_IE_QER_CORRELATION_ID                   (28)
#define SX_IE_PRECEDENCE                           (29)
#define SX_IE_TRANSPORT_LEVEL_MARKING              (30)
#define SX_IE_VOLUME_THRESHOLD                     (31)
#define SX_IE_TIME_THRESHOLD                       (32)
#define SX_IE_MONITORING_TIME                      (33)
#define SX_IE_SUBSEQUENT_VOLUME_THRESHOLD          (34)
#define SX_IE_SUBSEQUENT_TIME_THRESHOLD            (35)
#define SX_IE_INACTIVITIY_DETECTION_TIME           (36)
#define SX_IE_REPORTING_TRIGGERS                   (37)
#define SX_IE_REDIRECT_INFORMATION                 (38)
#define SX_IE_REPORT_TYPE                          (39)
#define SX_IE_OFFENDING_IE                         (40)
#define SX_IE_FORWARDING_POLICY                    (41)
#define SX_IE_DESTINATION_INTERFACE                (42)
#define SX_IE_UP_FUNCTION_FEATURES                 (43)
#define SX_IE_APPLY_ACTION                         (44)
#define SX_IE_DOWNLINK_DATA_SERVICE_INFORMATION    (45)
#define SX_IE_DOWNLINK_DATA_NOTIFICATION_DELAY     (46)
#define SX_IE_DL_BUFFERING_DURATION                (47)
#define SX_IE_DL_BUFFERING_SUGGESTED_PACKET_COUNT  (48)
#define SX_IE_SXSMREQ_FLAGS                        (49)
#define SX_IE_SXSRES_FLAGS                         (50)
#define SX_IE_LOAD_CONTROL_INFORMATION             (51)
#define SX_IE_SEQUENCE_NUMBER                      (52)
#define SX_IE_METRIC                               (53)
#define SX_IE_OVERLOAD_CONTROL_INFORMATION         (54)
#define SX_IE_TIMER                                (55)
#define SX_IE_PACKET_DETECTION_RULE_ID             (56)
#define SX_IE_F_SEID                               (57)
#define SX_IE_APPLICATION_ID_PFD                   (58)
#define SX_IE_PFD_CONTEXT                          (59)
#define SX_IE_NODE_ID                              (60)
#define SX_IE_PFD_CONTENTS                         (61)
#define SX_IE_MEASUREMENT_METHOD                   (62)
#define SX_IE_USAGE_REPORT_TRIGGER                 (63)
#define SX_IE_MEASUREMENT_PERIOD                   (64)
#define SX_IE_FQ_CSID                              (65)
#define SX_IE_VOLUME_MEASUREMENT                   (66)
#define SX_IE_DURATION_MEASUREMENT                 (67)
#define SX_IE_APPLICATION_DETECTION_INFORMATION    (68)
#define SX_IE_TIME_OF_FIRST_PACKET                 (69)
#define SX_IE_TIME_OF_LAST_PACKET                  (70)
#define SX_IE_QUOTA_HOLDING_TIME                   (71)
#define SX_IE_URR_ID                               (81)
#define SX_IE_OUTER_HEADER_CREATION                (84)
#define SX_IE_CP_FUNCTION_FEATURES                 (89)
#define SX_IE_OUTER_HEADER_REMOVAL                 (95)               
#define SX_IE_RECOVERY_TIME_STAMP                  (96)
#define SX_IE_ACTIVATE_PREDEFINED_RULES            (106)
#define SX_IE_FAR_ID                               (108)
#define SX_IE_QER_ID                               (109)
#define SX_IE_UP_IP_RESOURCE_INFORMATION           (116)





/**
 * Information Element structure as defined by 3GPP TS 29.244, clause 8.2.65, as
 * shown by Figure 8.2.65-1. 
 *
 * Sx_Recovery_time_ie.
 */
typedef struct sx_ie_t {
	uint16_t type;
	uint16_t length;
	uint8_t  *value;
	
} sx_ie;


enum interface_value{
      Access_nw=0, 
      Core_nw=1,
      Sgi_lan=2,
      Cp_function=3,
      
    
};

enum rule_id_type{
      PDR=0,
      FAR=1,
      QER=2,
      URR=3,
      BAR=4,
};

enum outer_header_creation_description{
       GTP_U_UDP_IPV4=0,
       GTP_U_UDP_IPV6=1,
       UDP_IPV4=2,
       UDP_IPV6=3,      

     };


 


/*
* There are missing many IEs.
*
*
*
*/

#endif /* __Sx_IE_H__ */





