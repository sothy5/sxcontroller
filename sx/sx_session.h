/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */


//#ifndef SX_SESSION_H
//#define SX_SESSION_H


#ifdef __cplusplus
extern "C" {
#endif

#include "sx_ie.h"
#include "sx_msg.h"
 

int create_sx_session_establishment_request(char* msg);

int process_sx_session_establishment_request(char* msg);

int create_sx_session_establishment_response(pfcp_header *header);

int process_sx_session_establishment_response(char* msg);

int create_sx_session_modification_request(pfcp_header *header);

int process_sx_session_modification_request(pfcp_header *header, pfcp_header *header_tx);

int create_sx_session_deletion_request(pfcp_header *header);

int process_sx_session_deletion_request(pfcp_header *header, pfcp_header *header_tx);


#ifdef __cplusplus
}
#endif




//#endif /* SX_SESSION_H */



