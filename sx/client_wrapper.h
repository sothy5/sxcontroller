/*
 * Copyright(c) 2019 Sivasothy SHANMUGALINGAM sothy.e98@gmail.com
 */


#include "sx_set_ie.h"
#include "sx_msg.h"


#ifdef __cplusplus
extern "C" {
#endif 

void  get_sx_heart_beat_msg(char* msg );

#ifdef __cplusplus
}
#endif
